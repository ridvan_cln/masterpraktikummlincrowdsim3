import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import matplotlib.animation as animation

xmin, xmax = -4, 4
mu_min, mu_max = -1, 1
fig = plt.figure()
ax = plt.axes(xlim=(xmin, xmax), ylim=(xmin, xmax))
line, = ax.plot([], [], lw=2)
ax.plot([xmin, xmax],[0,0], 'm')
ax.plot([0,0],[xmin, xmax], 'm')

def init():
    line.set_data([], [])
    return line,

def animate(mu):
    x = np.linspace(mu_min, mu_max, 100)
    y = mu - x**2
    line.set_data(x, y)
    return line,

bifurcation = animation.FuncAnimation(fig, animate, init_func=init, frames=np.linspace(mu_min, mu_max, 1000), interval=10, blit=True)

plt.xlabel('x', fontsize=15)
plt.ylabel('y', fontsize=15)
plt.tick_params(labelsize=15)

plt.show()