import numpy as np
from scipy import integrate
import sympy as sm

class ode:
    def __init__(self, alpha=0.1, dt=0.01, tstart=0, tend=100.0):
        self.dt = dt
        self.tstart = tstart
        self.tend = tend
        self.alpha = alpha

        self.sigma = 10
        self.beta = (8/3)
        self.rho = 28

    def equilibrium_task1(self):
        r, c = sm.symbols('r, c', negative=False)
        R = self.alpha*r + self.alpha*c
        C = -0.25*r + 0*c

        REqual = sm.Eq(R, 0)
        CEqual = sm.Eq(C, 0)

        return sm.solve( (REqual, CEqual), r, c )

    @staticmethod
    def evolute_task1(t, X, alpha):
        return  np.array([ alpha*X[0] + alpha*X[1], -0.25*X[0] + 0*X[1] ])

    @staticmethod
    def evolute_task2a(t, X, alpha):
        return np.array([ alpha - np.power(X[0],2), X[1]])
    
    def evolute_task2b(self, t, X, alpha):
        return np.array([ alpha - 2*np.power(X[0],2) - 3, X[1]])

    @staticmethod
    def evolute_task3(t, X, alpha):
        return np.array([ alpha*X[0] - X[1] - X[0]*(np.power(X[0],2)+np.power(X[1],2)), X[0] +alpha*X[1] - X[1]*(np.power(X[0],2)+np.power(X[1],2))])

    @staticmethod
    def evolute_task4_lorenz(t, X, sigma, beta, rho):
        return np.array([sigma*(X[1] - X[0]), rho*X[0] - X[1] - X[0]*X[2], X[0]*X[1] - beta*X[2]])

    def evolute_task4_logistic_map(self, t, X):
        return np.array([ self.alpha*X[0]*(1-X[0]) ])

    def integrate_task1_scipy(self, alpha, x0):
        return integrate.solve_ivp(ode.evolute_task1, 
                                    [self.tstart, self.tend], 
                                    y0=x0, 
                                    t_eval=np.linspace(self.tstart, self.tend, int(1/self.dt)), 
                                    args=([alpha]))

    def integrate_task2a_scipy(self, alpha, x0):
        return integrate.solve_ivp(ode.evolute_task2a, [self.tstart, self.tend], y0=x0, t_eval=np.linspace(self.tstart, self.tend, int(1/self.dt)), args=([alpha]))
    
    def integrate_task2b_scipy(self, alpha, x0):
        return integrate.solve_ivp(ode.evolute_task2b, [self.tstart, self.tend], x0, t_eval=np.linspace(self.tstart, self.tend, int(1/self.dt)), args=([alpha]))

    def integrate_task3_scipy(self, alpha, x0):
        return integrate.solve_ivp(ode.evolute_task3, 
                                    [self.tstart, self.tend], 
                                    x0, 
                                    t_eval=np.linspace(self.tstart, self.tend, int(1/self.dt)),
                                    args=([alpha]))

    def integrate_task4_scipy_lorzenz(self, x0, s=10, b=(8/3), r=28):
        return integrate.solve_ivp(ode.evolute_task4_lorenz, 
                                    [self.tstart, self.tend], 
                                    x0, 
                                    t_eval=np.linspace(self.tstart, self.tend, int(1/self.dt)),
                                    args=([s,b,r]))
    
    def integrate_task4_logistic_map(self, x0):
        return integrate.solve_ivp(self.evolute_task4_logistic_map, [self.tstart, self.tend], x0, t_eval=np.linspace(self.tstart, self.tend, int(1/self.dt)))

    def bifurcation_task2a (self):
        return np.sqrt(self.alpha), -np.sqrt(self.alpha)

    def bifurcation_task2b (self):
        return np.sqrt(-(1/2)*(3-self.alpha)), -np.sqrt(-(1/2)*(3-self.alpha))


def main():
    pass

if __name__ == '__main__':
    main()
    