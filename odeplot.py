import matplotlib.pyplot as plt
import numpy as np

from matplotlib.animation import FuncAnimation

from ode import ode

class odeplot:
    def __init__(self, row=1, col=2):
        self.row = row
        self.col = col
        self.pic = 1

    def plot_time_dynamics(self, fig, X, color, label):
        ax = fig.add_subplot(self.row,self.col,self.pic)
        for y,c,l in zip(X.y,color,label):
            ax.plot(X.t, y, c, label=l)
        ax.set_title("Dynamics in time")
        ax.set_xlabel("time")

        self.pic += 1

    def plot_phase_2D(self, fig, X, color):
        ax = fig.add_subplot(self.row,self.col,self.pic)
        x, y = X.y
        ax.plot(x,y, color=color)
        ax.set_title("Phase space")
        self.pic += 1

    def plot_phase_portrait_2D(self, fig, X, color, func, alpha, x_min=-1, x_max=1, y_min=-1, y_max=+1):
        ax = fig.add_subplot(self.row,self.col,self.pic)
        x, y = X.y

        x_mesh = np.linspace(x_min, x_max, 20)
        y_mesh = np.linspace(y_min, y_max, 20)

        X1 , Y1  = np.meshgrid(x_mesh, y_mesh) # create a grid
        DX1, DY1 = getattr(ode,func)(t=0, X=[X1, Y1], alpha=alpha)  # compute growth rate on the grid
        M = (np.hypot(DX1, DY1))               # norm growth rate 
        M[ M == 0] = 1.                        # avoid zero division errors 
        DX1 /= M                               # normalize each arrows
        DY1 /= M

        axes = plt.gca()
        axes.set_xlim([x_min,x_max])
        axes.set_ylim([y_min,y_max])

        ax.quiver(X1, Y1, DX1, DY1, M, pivot='mid')
        ax.plot(x, y, color=color)
        ax.set_title("Phaseportrait")

        self.pic += 1

    def plot_streamplot_2D(self, fig, func, alpha, x_min=-1, x_max=1, y_min=-1, y_max=+1, linewidth=2):
        ax = fig.add_subplot(self.row,self.col,self.pic)
        Y, X = np.mgrid[x_min:x_max:100j, y_min:y_max:100j]
        U, V = getattr(ode,func)(t=0, X=[X,Y], alpha=alpha)
        speed = np.sqrt(U**2 + V**2)
        strm = ax.streamplot(X, Y, U, V, color=speed, linewidth=2, cmap=plt.cm.jet)
        fig.colorbar(strm.lines)
        ax.set_title("Streamplot")
        self.pic += 1

    def plot_eigenvalues(self, fig, matrix):
        ax = fig.add_subplot(self.row,self.col,self.pic)
        eigenvalues = np.array([np.sort(np.linalg.eigvals([matrix]))])
        ax.scatter(eigenvalues[0].real, eigenvalues[0].imag)
        ax.set_xlabel('Real part')
        ax.set_ylabel('Imaginary part')
        ax.set_title('Eigenvalues')
        self.pic += 1

    def plot_lorenz(self, fig, X):
        x,y,z = X.y
        ax = fig.gca(projection='3d')
        ax.plot(x, y, z, lw=0.5)
        ax.set_xlabel("X Axis")
        ax.set_ylabel("Y Axis")
        ax.set_zlabel("Z Axis")
        ax.set_title("Lorenz Attractor") 
        self.pic += 1
    
    def plot_bifurcation(self, fig, xeq1, xeq2, alpha_start, alpha_end, eq_start, eq_end, domain, r: int):
        #fig = plt.figure(figsize=(9,6))
        ax1 = fig.add_subplot(self.row,self.col,self.pic)
        #domain = np.linspace(-1, 1)
        ax1.plot(domain, xeq1, 'b-', label = 'stable equilibrium', linewidth = 3)
        ax1.plot(domain, xeq2, 'r--', label = 'unstable equilibrium', linewidth = 3)
        ax1.legend(loc='upper left')
        #neutral equilibrium point
        ax1.plot([r], [0], 'go')
        ax1.axis([alpha_start, alpha_end, eq_start, eq_end])
        ax1.set_xlabel('alpha')
        ax1.set_ylabel('x_eq')
        ax1.set_title('Saddle-node bifurcation')
        self.pic += 1

def main():
    pass

if __name__ == '__main__':
    main()